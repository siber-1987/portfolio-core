properties {
    $baseDir = resolve-path .

    $dbScriptsDir = "$baseDir\database\scripts"
    $createDatabaseScript = "$dbScriptsDir\Create\001.CreateTableTransactions.sql"
    $aliaSqlPath = "$baseDir\database\AliaSQL.exe"
	$databaseName = "Portfolio"
	
	# localhost db
    #$databaseServer = "localhost"
    
	# db in container
    $databaseServer = "localhost,1433"
    $databaseUserName = "sa"
    $databasePwd = "p-aSSw0rd"
}

task init -depends publish, build-docker, start-sqlserver, build-db, run-docker

task publish {
    dotnet publish ./src/Portfolio.Api/Portfolio.Api.csproj -c Release -o ./src/Portfolio.Api/out
    dotnet publish ./src/Portfolio.Agent/Portfolio.Agent.csproj -c Release -o ./src/Portfolio.Agent/out
}

task build-docker {
    docker build -t "portfolio-api:latest" -f .\src\Portfolio.Api\Dockerfile .
    docker build -t "portfolio-agent:latest" -f .\src\Portfolio.Agent\Dockerfile .
}

task run-docker {
    docker-compose up -d
}

task build-db {
	Drop-Database
    Write-Host "Starting Building Database"
    Create-Database
    #Run-AliaSQL("Create")
}

task update-db {
    Write-Host "Starting Updating Database"
    Run-AliaSQL("Update")
}

task start-sqlserver {
    #exec { 'docker run -e "ACCEPT_EULA=Y" -e "SA_PASSWORD=$databasePwd" -p 1433:1433 -d mcr.microsoft.com/mssql/server:2019-latest' }
    docker-compose up -d sql
}

task install-modules {
    Install-PackageProvider -Name NuGet -MinimumVersion 2.8.5.201 -Force
    Set-PSRepository PSGallery -InstallationPolicy Trusted

    If(-not(Get-InstalledModule SqlServer -ErrorAction silentlycontinue)){
        Install-Module SqlServer -Confirm:$False -Force
    }
}

function Drop-Database {
	Drop-Database-Connections
    $query = "IF DB_ID('$databaseName') IS NOT NULL DROP DATABASE $databaseName"
    Run-Query-On-Server($query)
}

function Create-Database {
    $query = "CREATE DATABASE $databaseName;"
    Run-Query-On-Server($query)
    Run-Query-From-File($createDatabaseScript)
}

function Drop-Database-Connections {
	$query = "IF DB_ID('$databaseName') IS NOT NULL ALTER DATABASE $databaseName SET SINGLE_USER WITH ROLLBACK IMMEDIATE"
    Run-Query-On-Server($query)
	
	$query = "IF DB_ID('$databaseName') IS NOT NULL ALTER DATABASE $databaseName SET MULTI_USER"
    Run-Query-On-Server($query)
}

function Run-AliaSQL($action) {
    #exec { & $aliaSqlPath $action $databaseServer $databaseName $dbScriptsDir $databaseUserName $databasePwd }
	exec { & $  $action $databaseServer $databaseName $dbScriptsDir }
}

function Run-Query($query) {
    Import-Module "SqlServer" -DisableNameChecking
    Invoke-Sqlcmd -Query $query -serverinstance "$databaseServer" -database "$databaseName" -TrustServerCertificate $true | out-null
}

function Run-Query-From-File($queryFile) {
    Import-Module "SqlServer" -DisableNameChecking
    Invoke-Sqlcmd -InputFile $queryFile -serverinstance "$databaseServer" -UserName $databaseUserName -Password $databasePwd -database "$databaseName" -TrustServerCertificate | out-null
}

function Run-Query-On-Server($query) {
    Import-Module "SqlServer" -DisableNameChecking
    Invoke-Sqlcmd -Query $query -serverinstance "$databaseServer" -UserName $databaseUserName -Password $databasePwd -TrustServerCertificate | out-null
	#Invoke-Sqlcmd -Query $query -serverinstance "$databaseServer" -TrustServerCertificate $true | out-null
}
