﻿//using Microsoft.AspNetCore.Cors;
using Microsoft.AspNetCore.Mvc;
using Portfolio.Core.Models;
using Portfolio.Core.Services;

namespace Portfolio.Api.Controllers
{
    [ApiController]
    [Route("api/[controller]")]
    public class AssetsController : ControllerBase
    {
        private readonly IAssetsService _assetsService;

        public AssetsController(IAssetsService assetsService)
        {
            _assetsService = assetsService;
        }

        public IActionResult GetAssets(AssetFilter filter)
        {
            var assets = _assetsService.GetAssets(filter);

            return Ok(assets);
        }

        [HttpGet, Route("{id}/history")]
        public IActionResult GetAssetHistory(int id)
        {
            var history = _assetsService.GetAssetHistory(id);

            return Ok(history);
        }
    }
}
