﻿using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Identity.Web.Resource;

namespace Portfolio.Api.Controllers
{
    [Authorize]
    //[RequiredScope("portfolio-read")]
    [Route("api/[controller]")]
    [ApiController]
    public class TestController : ControllerBase
    {
        public IActionResult GetValue()
        {
            return Ok(5);
        }
    }
}
