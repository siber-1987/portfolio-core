﻿//using Microsoft.AspNetCore.Cors;
using Microsoft.AspNetCore.Mvc;
using Portfolio.Core.Models;
using Portfolio.Core.Services;

namespace Portfolio.Api.Controllers
{
    [ApiController]
    [Route("api/[controller]")]
    //[EnableCors("AllowAny")]
    public class TransactionsController : ControllerBase
    {
        private readonly ITransactionsService _transactionService;

        public TransactionsController(ITransactionsService transactionService)
        {
            _transactionService = transactionService;
        }

        public IActionResult GetTransactions(TransactionFilter filter)
        {
            var transactions = _transactionService.GetTransactions(filter);

            return Ok(transactions);
        }

        [HttpPost]
        public IActionResult GetTransactionAggregate(TransactionAggregateRequest filter)
        {
            var result = _transactionService.GetTransactionAggregate(filter);

            return Ok(result);
        }
    }
}
