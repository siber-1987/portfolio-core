﻿using System;
using System.IO;

namespace Portfolio.Agent.Utils
{
    public class FileHelper
    {
        public void CreateSubfolders(string path)
        {
            var inProgressPath = Path.Combine(path, Subfolders.InProgress);
            var completedPath = Path.Combine(path, Subfolders.Completed);

            Directory.CreateDirectory(inProgressPath);
            Directory.CreateDirectory(completedPath);
        }

        public string AppendDateTimeToFileName(string filePath)
        {
            var dateTimeStr = DateTime.Now.ToString("yyyy-MM-dd-HH-mm-ss");
            var directory = Path.GetDirectoryName(filePath);
            var fileName = Path.GetFileNameWithoutExtension(filePath);
            var extension = Path.GetExtension(filePath);

            var newFileName = $"{fileName}-{dateTimeStr}{extension}";
            var newFilePath = Path.Combine(directory, newFileName);

            File.Move(filePath, newFilePath);

            return newFilePath;
        }

        public string MoveTo(string filePath, string directory, string subfolder)
        {
            var newFilePath = RemoveSubfolderFromPath(filePath);

            var fileName = Path.GetFileName(newFilePath);
            newFilePath = Path.Combine(directory, subfolder, fileName);

            Console.WriteLine($"filePath={filePath}, subfolder={subfolder}, directory={directory}, fileName={fileName}, newFilePath={newFilePath} ");

            File.Move(filePath, newFilePath);

            return newFilePath;
        }

        private string RemoveSubfolderFromPath(string filePath)
        {
            var subfolders = new string[] { Subfolders.Completed, Subfolders.InProgress };
            var result = filePath;

            foreach (var subfolder in subfolders)
            {
                if (result.Contains(subfolder))
                {
                    result = result.Replace(subfolder + "\\", string.Empty);
                }
            }

            return result;
        }
    }
}
