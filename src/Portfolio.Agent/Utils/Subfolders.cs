﻿namespace Portfolio.Agent.Utils
{
    internal class Subfolders
    {
        public static string InProgress => "InProgress";
        public static string Completed => "Completed";
    }
}
