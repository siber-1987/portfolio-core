﻿using System;
using System.IO;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;
using Portfolio.Agent.Utils;
using Portfolio.Core.Tasks;
using Quartz;

namespace Portfolio.Agent.Jobs
{
    [DisallowConcurrentExecution]
    internal class TransactionExtractLoaderJob : IJob
    {
        private string _mainDirectory;
        private string _searchPattern;
        private FileHelper _fileHelper;
        private IImportTransactionsTask _importTransactionsTask;
        private IConfiguration _configuration;

        public TransactionExtractLoaderJob()
        {
        }

        private void Init(IServiceProvider serviceProvider)
        {
            _configuration = serviceProvider.GetService<IConfiguration>();
            _importTransactionsTask = serviceProvider.GetService<IImportTransactionsTask>();

            _mainDirectory = _configuration["TransactionsExtractDirectory"];
            _searchPattern = _configuration["SearchPattern"];

            _fileHelper = new FileHelper();
            _fileHelper.CreateSubfolders(_mainDirectory);

            Console.WriteLine($"Waiting for transaction extract files in: {_mainDirectory}, search pattern={_searchPattern}");
        }

        public async Task Execute(IJobExecutionContext context)
        {
            var serviceProvider = context.JobDetail.JobDataMap.Get("serviceProvider") as IServiceProvider;

            Init(serviceProvider);

            var originalFilePath = Directory.EnumerateFiles(_mainDirectory, _searchPattern, SearchOption.TopDirectoryOnly).FirstOrDefault();

            if (originalFilePath != null)
            {
                try
                {
                    var originalFileName = Path.GetFileName(originalFilePath);
                    Console.WriteLine($"Start loading file {originalFileName}");

                    var inProgressFilePath = _fileHelper.AppendDateTimeToFileName(originalFilePath);
                    inProgressFilePath = _fileHelper.MoveTo(inProgressFilePath, _mainDirectory, Subfolders.InProgress);

                    Console.WriteLine($"Moved file to {inProgressFilePath}");

                    _importTransactionsTask.Run(inProgressFilePath);
                    Console.WriteLine($"File {originalFileName} loaded");

                    var completedFilePath = _fileHelper.MoveTo(inProgressFilePath, _mainDirectory, Subfolders.Completed);
                }
                catch (Exception e)
                {
                    Console.WriteLine(e.Message);
                    await Console.Out.WriteLineAsync(e.StackTrace);
                }
            }

            await Console.Out.WriteLineAsync("No files to load");
        }


    }
}