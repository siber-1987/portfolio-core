﻿using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;
using Portfolio.Agent.Utils;
using Portfolio.Core.Tasks;
using Quartz;
using System;
using System.IO;
using System.Linq;
using System.Threading.Tasks;

namespace Portfolio.Agent.Jobs
{
    [DisallowConcurrentExecution]
    internal class ImportSplitMergeHistoryJob : IJob
    {
        private string _mainDirectory;
        private string _searchPattern;
        private FileHelper _fileHelper;
        private IImportSplitMergeHistoryTask _importSplitMergeHistoryTask;
        private IConfiguration _configuration;

        public ImportSplitMergeHistoryJob()
        {
        }

        private void Init(IServiceProvider serviceProvider)
        {
            _configuration = serviceProvider.GetService<IConfiguration>();
            _importSplitMergeHistoryTask = serviceProvider.GetService<IImportSplitMergeHistoryTask>();

            _mainDirectory = _configuration["ImportSplitMergeHistoryDirectory"];
            _searchPattern = _configuration["SearchPattern"];

            _fileHelper = new FileHelper();
            _fileHelper.CreateSubfolders(_mainDirectory);

            Console.WriteLine($"Waiting for transaction extract files in: {_mainDirectory}, search pattern={_searchPattern}");
        }

        public async Task Execute(IJobExecutionContext context)
        {
            try
            {
                var serviceProvider = context.JobDetail.JobDataMap.Get("serviceProvider") as IServiceProvider;

                Init(serviceProvider);

                var originalFilePath = Directory.EnumerateFiles(_mainDirectory, _searchPattern, SearchOption.TopDirectoryOnly).FirstOrDefault();

                if (originalFilePath != null)
                {
                    try
                    {
                        var originalFileName = Path.GetFileName(originalFilePath);
                        Console.WriteLine($"Start loading file {originalFileName}");

                        var inProgressFilePath = _fileHelper.AppendDateTimeToFileName(originalFilePath);
                        inProgressFilePath = _fileHelper.MoveTo(inProgressFilePath, _mainDirectory, Subfolders.InProgress);

                        Console.WriteLine($"Moved file to {inProgressFilePath}");

                        _importSplitMergeHistoryTask.Run(inProgressFilePath);
                        Console.WriteLine($"File {originalFileName} loaded");

                        var completedFilePath = _fileHelper.MoveTo(inProgressFilePath, _mainDirectory, Subfolders.Completed);
                    }
                    catch (Exception e)
                    {
                        Console.WriteLine(e.Message);
                        await Console.Out.WriteLineAsync(e.StackTrace);
                    }
                }

                await Console.Out.WriteLineAsync("No files to load");
            }
            catch (Exception ex)
            {
                await Console.Out.WriteLineAsync("Error occurred");
                await Console.Out.WriteLineAsync(ex.Message);
                await Console.Out.WriteLineAsync(ex.StackTrace);
            }
        }


    }
}