﻿using System;
using System.Collections.Specialized;
using System.Threading.Tasks;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;
using Portfolio.Agent.Jobs;
using Quartz;
using Quartz.Impl;

namespace Portfolio.Agent
{
    internal class AgentService
    {
        private IScheduler _scheduler;
        private readonly int _transactionExtractLoaderJobInterval;
        private readonly int _importSplitMergeHistoryJobInterval;
        private readonly IServiceProvider _serviceProvider;
        private readonly IConfiguration _configuration;

        public AgentService(IServiceProvider serviceProvider)
        {
            _serviceProvider = serviceProvider;

            _configuration = serviceProvider.GetService<IConfiguration>();
            _transactionExtractLoaderJobInterval = int.Parse(_configuration["TransactionExtractLoaderJobInterval"]);
            _importSplitMergeHistoryJobInterval = int.Parse(_configuration["ImportSplitMergeHistoryJobInterval"]);

            InitScheduler().GetAwaiter().GetResult();
            CreateJob().GetAwaiter().GetResult();
        }

        public void Start()
        {
            Console.WriteLine("Start");

            StartScheduler().GetAwaiter().GetResult();
        }

        public void Stop()
        {
            Console.WriteLine("Stop");

            StopScheduler().GetAwaiter().GetResult();
        }

        private async Task InitScheduler()
        {
            NameValueCollection props = new NameValueCollection
                {
                    { "quartz.serializer.type", "binary" }
                };

            StdSchedulerFactory factory = new StdSchedulerFactory(props);

            _scheduler = await factory.GetScheduler();
        }

        private async Task CreateJob()
        {
            var jobData = new JobDataMap();
            jobData.Add("serviceProvider", _serviceProvider);

            IJobDetail transactionExtractLoaderJob = JobBuilder.Create<TransactionExtractLoaderJob>()
                .WithIdentity("TransactionExtractLoaderJob", "TransactionExtractLoaderJobGroup")
                .UsingJobData(jobData)
                .Build();

            ITrigger transactionExtractLoaderJobTrigger = TriggerBuilder.Create()
                .WithIdentity("transactionExtractLoaderJobTrigger", "transactionExtractLoaderJobTriggerGroup")
                .StartNow()
                .WithSimpleSchedule(x => x
                    .WithIntervalInSeconds(_transactionExtractLoaderJobInterval)
                    .RepeatForever())
                .Build();

            IJobDetail importSplitMergeHistoryJob = JobBuilder.Create<ImportSplitMergeHistoryJob>()
                .WithIdentity("ImportSplitMergeHistoryJob", "ImportSplitMergeHistoryJobGroup")
                .UsingJobData(jobData)
                .Build();

            ITrigger importSplitMergeHistoryJobTrigger = TriggerBuilder.Create()
                .WithIdentity("importSplitMergeHistoryJobTrigger", "importSplitMergeHistoryJobTriggerGroup")
                .StartNow()
                .WithSimpleSchedule(x => x
                    .WithIntervalInSeconds(_importSplitMergeHistoryJobInterval)
                    .RepeatForever())
                .Build();

            await _scheduler.ScheduleJob(transactionExtractLoaderJob, transactionExtractLoaderJobTrigger);
            await _scheduler.ScheduleJob(importSplitMergeHistoryJob, importSplitMergeHistoryJobTrigger);
        }

        private async Task StartScheduler()
        {
            try
            {
                await _scheduler.Start();
            }
            catch (SchedulerException e)
            {
                await Console.Error.WriteLineAsync(e.ToString());
            }
        }

        private async Task StopScheduler()
        {
            try
            {
                await _scheduler.Shutdown();
            }
            catch (SchedulerException e)
            {
                await Console.Error.WriteLineAsync(e.ToString());
            }
        }

    }
}
