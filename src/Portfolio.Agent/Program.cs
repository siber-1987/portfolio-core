﻿using System;
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;
using Portfolio.Agent.Jobs;
using Portfolio.Core;
using Portfolio.Data;
using Quartz;

namespace Portfolio.Agent
{
    class Program
    {
        static void Main(string[] args)
        {
            IConfiguration configuration = new ConfigurationBuilder()
                .AddJsonFile("appsettings.json", optional: true, reloadOnChange: true)
                .AddEnvironmentVariables()
                .Build();

            var services = new ServiceCollection();
            services.AddSingleton(configuration);
            services.AddTransient<TransactionExtractLoaderJob>();
            services.AddPortfolioCoreServices();
            services.AddPortfolioDataServices(configuration);

            var serviceProvider = services.BuildServiceProvider();
            serviceProvider.GetService<string>();

            AddDbContext(services, configuration["ConnectionString"]);

            var agentService = new AgentService(serviceProvider);
            agentService.Start();

            Console.In.ReadLine();

            agentService.Stop();
        }

        private static void AddDbContext(IServiceCollection services, string connectionString)
        {
            services.AddDbContext<PortfolioContext>(options =>
                {
                    options.UseSqlServer(connectionString,
                        sqlServerOptionsAction: sqlOptions =>
                        {
                            sqlOptions.EnableRetryOnFailure(maxRetryCount: 10, maxRetryDelay: TimeSpan.FromSeconds(30), errorNumbersToAdd: null);
                        });
                });
        }
    }
}
