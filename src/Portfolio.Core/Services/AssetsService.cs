﻿using System.Collections.Generic;
using System.Linq;
using Portfolio.Core.Models;
using Portfolio.Data.Repositories;

namespace Portfolio.Core.Services
{
    public interface IAssetsService
    {
        IEnumerable<Asset> GetAssets(AssetFilter filter = null);
        IEnumerable<AssetHistoryEntry> GetAssetHistory(int id);
    }

    public class AssetsService : IAssetsService
    {
        private readonly ITransactionsRepository _transactionsRepository;
        private readonly ITransactionsService _transactionsService;


        public AssetsService(ITransactionsRepository transactionsRepository,
            ITransactionsService transactionsService)
        {
            _transactionsRepository = transactionsRepository;
            _transactionsService = transactionsService;
        }



        public IEnumerable<AssetHistoryEntry> GetAssetHistory(int id)
        {
            var filter = new TransactionFilter
            {
                AssetId = id
            };

            // Shares currently in portfolio currently
            //var transactions = _transactionsService.GetTransactions(transactionsFilter)
            //    .Where(t => t.AssetId == id
            //        && t.SoldCounter != t.Volume
            //        && t.TransactionType == TransactionType.Buy)
            //    .ToList();

            //var cnt = transactions.Sum(x => x.Volume - x.SoldCounter);

            var history = new List<AssetHistoryEntry>();
            var volumeCnt = 0;
            var transactions = _transactionsService.GetTransactions(filter).GroupBy(x => x.TransactionTime.Date);
            foreach (var group in transactions)
            {
                var buyVolume = group.Where(x => x.TransactionType == TransactionType.Buy).Sum(y => y.Volume);
                var sellVolume = group.Where(x => x.TransactionType == TransactionType.Sell).Sum(y => y.Volume);

                volumeCnt += buyVolume - sellVolume;

                var entry = new AssetHistoryEntry()
                {
                    AssetId = id,
                    Date = group.Key,
                    Volume = volumeCnt
                };

                history.Add(entry);
            }


            return history;
        }

        public IEnumerable<Asset> GetAssets(AssetFilter filter = null)
        {
            var assetsQuery = _transactionsRepository.GetAssets()
                .Select(x => new Asset
                {
                    Id = x.Id,
                    AssetGroupId = x.AssetGroupId,
                    Code = x.Code,
                    Name = x.Name
                });

            if (filter != null)
            {
                if (!string.IsNullOrWhiteSpace(filter.AssetName))
                {
                    assetsQuery = assetsQuery.Where(x => x.Name.Contains(filter.AssetName));
                }
            }

            var assets = assetsQuery.AsEnumerable();

            return assets.OrderBy(x => x.Name);
        }
    }
}
