﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net.WebSockets;
using Portfolio.Core.Extensions;
using Portfolio.Core.Models;
using Portfolio.Core.Transactions;
using Portfolio.Data.Repositories;

namespace Portfolio.Core.Services
{
    public interface ITransactionsService
    {
        IEnumerable<Transaction> GetTransactions(TransactionFilter filter);
        IEnumerable<TransactionAggregate> GetTransactionAggregate(TransactionAggregateRequest filter);
    }

    public class TransactionsService : ITransactionsService
    {
        private ITransactionsRepository _transactionsRepository;
        private ICommissionCalculator _commissionCalculator;
        private ISplitMergeRepository _splitMergeRepository;

        public TransactionsService(
            ITransactionsRepository transactionsRepository,
            ICommissionCalculator commissionCalculator,
            ISplitMergeRepository splitMergeRepository
            )
        {
            _transactionsRepository = transactionsRepository;
            _commissionCalculator = commissionCalculator;
            _splitMergeRepository = splitMergeRepository;
        }

        public IEnumerable<TransactionAggregate> GetTransactionAggregate(TransactionAggregateRequest request)
        {
            var result = new List<TransactionAggregate>();
            var transactions = GetTransactions(request.TransactionFilter)
                .Where(x => x.TransactionType == TransactionType.Sell);

            // Build aggregate per transaction

            // Group by asset
            if (request.GroupByAsset)
            {
                foreach (var transaction in transactions)
                {
                    var aggregate = result.FirstOrDefault(x => x.AssetName == transaction.AssetName);
                    if (aggregate == null)
                    {
                        aggregate = new TransactionAggregate()
                        {
                            AssetName = transaction.AssetName,
                            AssetId = transaction.AssetId
                        };
                        result.Add(aggregate);
                    }

                    IncludeInAggregate(aggregate, transaction);
                }
            }

            // Group by period
            if (request.GroupByPeriod)
            {
                foreach (var transaction in transactions)
                {
                    var period = GetPeriod(transaction.TransactionTime, request.PeriodType);
                    if (!period.HasValue)
                    {
                        throw new Exception($"Could not resolve period for transaction id={transaction.Id}, transactionTime={transaction.TransactionTime}");
                    }

                    var aggregate = result.FirstOrDefault(x => x.Period == period);
                    if (aggregate == null)
                    {
                        aggregate = new TransactionAggregate()
                        {
                            Period = period.Value
                        };
                        result.Add(aggregate);
                    }

                    IncludeInAggregate(aggregate, transaction);
                }
            }

            // Order
            //result.OrderBy(x => x.AssetName);

            return result;
        }

        public IEnumerable<Transaction> GetTransactions(TransactionFilter filter)
        {
            var transactionsQuery = _transactionsRepository.GetTransactions();
            transactionsQuery = FilterTransactions(transactionsQuery, filter);

            var dbTransactions = transactionsQuery.ToList();

            var transactions = dbTransactions.Select(x => new Transaction
            {
                Id = x.Id,
                AssetId = x.AssetId,
                AssetName = x.Asset.Name,
                PriceInCurrency = x.PriceInCurrency,
                Currency = x.Currency,
                Amount = x.Amount,
                Commission = x.Commission,
                TransactionTime = x.TransactionTime,
                TransactionType = (TransactionType)x.TransactionType,
                Volume = x.Volume
            }).AsEnumerable();

            var groupedTransactions = transactions.GroupBy(x => x.AssetId);
            var result = new List<Transaction>();

            var allSplitMerges = _splitMergeRepository
                .GetAll()
                .AsEnumerable();

            foreach (var group in groupedTransactions)
            {
                var buyTransactions = group
                    .Where(x => x.TransactionType == TransactionType.Buy)
                    .OrderBy(y => y.TransactionTime);

                var sellTransactions = group
                    .Where(x => x.TransactionType == TransactionType.Sell)
                    .OrderBy(y => y.TransactionTime);

                var assetSplitMerges = allSplitMerges
                    .Where(x => x.AssetId == group.Key)
                    .AsEnumerable();

                foreach (var sellTransaction in sellTransactions)
                {
                    var volumeCounter = sellTransaction.Volume;
                    var buyAmountSum = 0m;

                    var availableBuyTransactions = buyTransactions.Where(x => x.SoldCounter < x.Volume);

                    // Foreach sell transaction look for corresponding buy transaction(s)
                    foreach (var buyTransaction in availableBuyTransactions)
                    {
                        if (buyTransaction.SoldCounter == buyTransaction.Volume) continue;

                        var splitMerge = FindMatchingSplitMerge(assetSplitMerges, buyTransaction.TransactionTime, sellTransaction.TransactionTime);
                        var splitMergeMultiplier = splitMerge != null
                            ? splitMerge.After / splitMerge.Before
                            : 1m;

                        var volumeAvailableToSell = buyTransaction.Volume - buyTransaction.SoldCounter;
                        var volumeToSell = (int)Math.Min(volumeAvailableToSell, volumeCounter / splitMergeMultiplier);

                        buyTransaction.SoldCounter += volumeToSell;
                        buyAmountSum += volumeToSell * buyTransaction.Price;

                        // Link buy trasnactions
                        if (sellTransaction.BuyTransactions == null)
                        {
                            sellTransaction.BuyTransactions = new List<Transaction>();
                        }
                        sellTransaction.BuyTransactions.Add(buyTransaction);

                        // Calculate BUY transaction balance only if it was sold in 1 transaction
                        // TODO: Take into account splitMergeMultiplier
                        /*if (buyTransaction.SoldCounter == buyTransaction.Volume)
                        {
                            buyTransaction.Balance = (sellTransaction.Price - buyTransaction.Price) * buyTransaction.Volume;
                        }*/

                        volumeCounter -= (int)(volumeToSell * splitMergeMultiplier);
                        if (volumeCounter == 0) break;
                    }

                    var buyCommission = _commissionCalculator.CalculateCommission(buyAmountSum);
                    sellTransaction.CommissionsSum = sellTransaction.Commission + buyCommission;
                    sellTransaction.Balance = sellTransaction.Amount - buyAmountSum - sellTransaction.CommissionsSum;
                }

                result.AddRange(group);
            }

            return result.OrderBy(x => x.TransactionTime);
        }

        public IEnumerable<Transaction> GetAssetTransactionsInPortfolio(int assetId)
        {
            var filter = new TransactionFilter
            {
                AssetId = assetId
            };

            var transactions = GetTransactions(filter)
                .Where(t => t.SoldCounter != t.Volume
                    && t.TransactionType == TransactionType.Buy)
                .ToList();

            var cnt = transactions.Sum(x => x.Volume - x.SoldCounter);

            return null;
        }

        private IQueryable<Data.Models.Transaction> FilterTransactions(IQueryable<Data.Models.Transaction> transactions, TransactionFilter filter)
        {
            if (filter == null)
                return transactions;

            if (filter.AssetId.HasValue && filter.AssetId.Value != 0)
            {
                transactions = transactions.Where(x => x.AssetId == filter.AssetId);
            }

            if (filter.DateFrom.HasValue)
            {
                transactions = transactions.Where(x => x.TransactionTime >= filter.DateFrom);
            }

            if (filter.DateTo.HasValue)
            {
                transactions = transactions.Where(x => x.TransactionTime <= filter.DateTo);
            }

            return transactions;
        }

        private void IncludeInAggregate(TransactionAggregate aggregate, Transaction transaction)
        {
            //var expenses = transaction.Volume * transaction.Price + transaction.CommissionsSum;
            var expenses = transaction.Amount + transaction.CommissionsSum;
            var income = expenses + transaction.Balance;
            aggregate.ExpensesSum += expenses;
            aggregate.IncomeSum += income;
            aggregate.Balance += transaction.Balance;
        }

        private DateTime? GetPeriod(DateTime dateTime, PeriodType periodType)
        {
            switch (periodType)
            {
                case PeriodType.Year:
                    return new DateTime(dateTime.Year, 1, 1);
                case PeriodType.Month:
                    return new DateTime(dateTime.Year, dateTime.Month, 1);
                case PeriodType.Day:
                    return new DateTime(dateTime.Year, dateTime.Month, dateTime.Day);
                default:
                    return null;
            }
        }

        private SplitMerge FindMatchingSplitMerge(IEnumerable<Portfolio.Data.Models.SplitMerge> splitMerges, DateTime firsTransactionDate, DateTime secondTransactionDate)
        {
            if (splitMerges == null || splitMerges.Count() == 0)
            {
                return null;
            }

            return splitMerges
                .Where(x =>
                    x.StateTime > firsTransactionDate &&
                    x.StateTime < secondTransactionDate
                    )
                .Select(x => new SplitMerge
                {
                    Id = x.Id,
                    AssetId = x.AssetId,
                    SplitMergeType = (SplitMergeType)x.SplitMergeType,
                    StateTime = x.StateTime,
                    ChangeTime = x.ChangeTime,
                    Before = x.Before,
                    After = x.After
                })
                .FirstOrDefault();
        }

    }
}
