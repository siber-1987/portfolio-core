﻿using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;
using Portfolio.Core.Services;
using Portfolio.Core.Tasks;
using Portfolio.Core.Tasks.ImportSplitMergeHistory;
using Portfolio.Core.Tasks.ImportTransactions;
using Portfolio.Core.Transactions;
using Portfolio.Core.Utils;

namespace Portfolio.Core
{
    public static class Installer
    {
        public static void AddPortfolioCoreServices(this IServiceCollection services)
        {
            services.AddTransient<IAssetsService, AssetsService>();
            services.AddTransient<ITransactionsService, TransactionsService>();
            services.AddTransient<ICommissionCalculator, CommissionCalculator>();
            services.AddTransient<IImportTransactionsTask, ImportTransactionsTask>();
            services.AddTransient<IImportSplitMergeHistoryTask, ImportSplitMergeHistoryTask>();
            services.AddTransient<ITransactionEntryParser, TransactionEntryParser>();
            services.AddTransient<ISplitMergeEntryParser, SplitMergeEntryParser>();
            services.AddTransient<IFileReader, FileReader>();
            services.AddTransient<IAssetGroupCreator, AssetGroupCreator>();
        }
    }
}
