﻿using Portfolio.Core.Models;

namespace Portfolio.Core.Transactions
{
    public interface ICommissionCalculator
    {
        decimal CalculateCommission(Transaction transaction);
        decimal CalculateCommission(decimal amount);
    }

    public class CommissionCalculator : ICommissionCalculator
    {
        private static decimal CommissionPercentage = 0.0039m;

        public decimal CalculateCommission(Transaction transaction)
        {
            return CommissionPercentage * transaction.PriceInCurrency * transaction.Volume;
        }

        public decimal CalculateCommission(decimal amount)
        {
            return CommissionPercentage * amount;
        }
    }
}
