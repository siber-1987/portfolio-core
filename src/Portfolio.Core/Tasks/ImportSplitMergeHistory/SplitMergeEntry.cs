﻿using Portfolio.Core.Models;
using System;

namespace Portfolio.Core.Tasks.ImportSplitMergeHistory
{
    public class SplitMergeEntry
    {
        public int Identifier { get; set; }

        public string AssetName { get; set; }

        public DateTime StateTime { get; set; }

        public DateTime ChangeTime { get; set; }

        public SplitMergeType SplitMergeType { get; set; }

        public int Before { get; set; }

        public int After { get; set; }
    }
}
