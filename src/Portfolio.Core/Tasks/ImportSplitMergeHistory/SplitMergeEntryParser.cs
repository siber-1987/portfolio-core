﻿using Portfolio.Core.Models;
using System;
using System.Globalization;

namespace Portfolio.Core.Tasks.ImportSplitMergeHistory
{
    public interface ISplitMergeEntryParser
    {
        SplitMergeEntry Parse(string line);
    }

    public class SplitMergeEntryParser : ISplitMergeEntryParser
    {
        private const string DateTimeFormat = "dd.MM.yyyy";

        public SplitMergeEntry Parse(string line)
        {
            var items = line.Split(';');
            var stateTime = DateTime.ParseExact(items[0], DateTimeFormat, CultureInfo.InvariantCulture);
            var changeTime = DateTime.ParseExact(items[1], DateTimeFormat, CultureInfo.InvariantCulture);
            var assetName = items[2];
            var type = GetSplitMergeType(items[4]);
            var before = int.Parse(items[5]);
            var after = int.Parse(items[6]);

            return new SplitMergeEntry
            {
                AssetName = assetName,
                StateTime = stateTime,
                ChangeTime = changeTime,
                SplitMergeType = type,
                Before = before,
                After = after
            };
        }

        private SplitMergeType GetSplitMergeType(string splitMergeTypeStr)
        {
            return splitMergeTypeStr == "S" ? SplitMergeType.Split : SplitMergeType.Merge;
        }
    }
}
