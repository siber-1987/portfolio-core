﻿using Portfolio.Core.Tasks.ImportSplitMergeHistory;
using Portfolio.Core.Utils;
using Portfolio.Data.Models;
using Portfolio.Data.Repositories;
using System;
using System.Linq;

namespace Portfolio.Core.Tasks
{
    public interface IImportSplitMergeHistoryTask
    {
        void Run(string fileName);
    }

    public class ImportSplitMergeHistoryTask : IImportSplitMergeHistoryTask
    {
        private readonly ISplitMergeRepository _splitMergeRepository;
        private readonly ITransactionsRepository _transactionRepository;
        private readonly ISplitMergeEntryParser _splitMergeEntryParser;
        private readonly IFileReader _fileReader;
        private readonly IAssetGroupCreator _assetGroupCreator;

        public ImportSplitMergeHistoryTask(  
            ISplitMergeRepository splitMergeRepository,
            ITransactionsRepository transactionRepository,
            ISplitMergeEntryParser splitMergeEntryParser,
            IFileReader fileReader,
            IAssetGroupCreator assetGroupCreator)
        {
            _splitMergeRepository = splitMergeRepository;
            _transactionRepository = transactionRepository;
            _splitMergeEntryParser = splitMergeEntryParser;
            _fileReader = fileReader;
            _assetGroupCreator = assetGroupCreator;
        }

        public void Run(string fileName)
        {
            var lines = _fileReader.ReadLines(fileName);

            int existingMaxIdentifier = _splitMergeRepository.GetAll().Any() ? _splitMergeRepository.GetAll().Max(x => x.Identifier) : 0;

            var importedEntries = lines
                .Skip(1)
                .Select(_splitMergeEntryParser.Parse)
                .Skip(existingMaxIdentifier)
                .ToList();

            var cnt = existingMaxIdentifier + 1;
            foreach (var entry in importedEntries)
            {
                entry.Identifier = cnt++;
            }

            var existingAssets = _transactionRepository.GetAssets().ToList();

            // TODO: What to do if asset name is not found in DB?
            var entriesToAdd = importedEntries.Select(x =>
            {
                var asset = existingAssets
                    .Where(a => a.Name == x.AssetName)
                    .FirstOrDefault()
                    ?? throw new Exception($"Can't find asset '{x.AssetName}'");

                return new SplitMerge
                {
                    Identifier = x.Identifier,
                    AssetId = asset.Id,
                    StateTime = x.StateTime,
                    ChangeTime = x.ChangeTime,
                    SplitMergeType = (int)x.SplitMergeType,
                    Before = x.Before,
                    After = x.After
                };
            }
            ).ToArray();

            _splitMergeRepository.Add(entriesToAdd);

            Console.WriteLine($"Imported splits/merges: {entriesToAdd.Length}");
        }

    }
}
