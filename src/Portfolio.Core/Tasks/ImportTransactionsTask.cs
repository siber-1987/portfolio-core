﻿using Portfolio.Core.Tasks.ImportTransactions;
using Portfolio.Core.Utils;
using Portfolio.Data.Models;
using Portfolio.Data.Repositories;
using System;
using System.Collections.Generic;
using System.Linq;

namespace Portfolio.Core.Tasks
{
    public interface IImportTransactionsTask
    {
        void Run(string fileName);
    }

    public class ImportTransactionsTask : IImportTransactionsTask
    {
        /// <summary>
        /// File is beeing read until this substring is found. Next lines should contain data.
        /// </summary>
        private const string LastLineIndicatorToTerminateCleaning = "Czas transakcji";

        private readonly ITransactionsRepository _transactionRepository;
        private readonly ITransactionEntryParser _transactionEntryParser;
        private readonly IFileReader _fileReader;
        private readonly IAssetGroupCreator _assetGroupCreator;

        public ImportTransactionsTask(
            ITransactionsRepository transactionRepository,
            ITransactionEntryParser transactionEntryParser,
            IFileReader fileReader,
            IAssetGroupCreator assetGroupCreator)
        {
            _transactionRepository = transactionRepository;
            _transactionEntryParser = transactionEntryParser;
            _fileReader = fileReader;
            _assetGroupCreator = assetGroupCreator;
        }

        public void Run(string fileName)
        {
            var lines = _fileReader.ReadLines(fileName);
            lines = Clear(lines.ToList());

            int existingMaxIdentifier = _transactionRepository.GetTransactions().Any() ? _transactionRepository.GetTransactions().Max(x => x.Identifier) : 0;

            var importedHistoryEntries = lines
                .Skip(1)
                .Select(_transactionEntryParser.Parse)
                .Reverse().
                Skip(existingMaxIdentifier)
                .ToList();

            var cnt = existingMaxIdentifier + 1;
            foreach (var entry in importedHistoryEntries)
            {
                entry.Identifier = cnt++;
            }

            var importedAssets = importedHistoryEntries.Select(x => x.AssetName)
                .Distinct()
                .Select(x => new Asset
                {
                    Code = x,
                    Name = x,
                });

            var importedAssetGroups = importedAssets.Select(x =>
                {
                    var code = _assetGroupCreator.GetGroupCode(x.Name);
                    var name = _assetGroupCreator.GetGroupName(x.Name);
                    var assetGroup = new AssetGroup
                    {
                        Code = code,
                        Name = name
                    };
                    return assetGroup;

                })
                .GroupBy(x => x.Code, (key, x) => x.FirstOrDefault());

            // Add Asset Groups
            var existingAssetGroups = _transactionRepository.GetAssetGroups().ToList();
            var assetGroupsToAdd = importedAssetGroups.Where(x => !existingAssetGroups.Any(g => g.Code == x.Code)).ToArray();
            _transactionRepository.AddAssetGroups(assetGroupsToAdd);

            // Add Assets
            var existingAssets = _transactionRepository.GetAssets().ToList();
            var assetsToAdd = importedAssets.Where(x => !existingAssets.Any(a => a.Code == x.Code)).ToArray();
            AttachAssetGroups(assetsToAdd);
            _transactionRepository.AddAssets(assetsToAdd);

            // Add Transactions
            existingAssets = _transactionRepository.GetAssets().ToList();
            var transactionsToAdd = importedHistoryEntries.Select(x => new Transaction
            {
                Identifier = x.Identifier,
                AssetId = existingAssets.Where(a => a.Name == x.AssetName).First().Id,
                TransactionTime = x.TransactionTime,
                TransactionType = (int)x.TransactionType,
                PriceInCurrency = x.PriceInCurrency,
                Volume = x.Volume,
                Amount = x.Amount,
                Commission = x.Commission,
                Currency = x.Currency
            }).ToArray();

            _transactionRepository.AddTransactions(transactionsToAdd);

            Console.WriteLine($"Imported transactions: {transactionsToAdd.Length}");
        }

        private void AttachAssetGroups(Asset[] assets)
        {
            var existingGroups = _transactionRepository.GetAssetGroups().ToList();

            foreach (var asset in assets)
            {
                var groupCode = _assetGroupCreator.GetGroupCode(asset.Name);
                var group = existingGroups.FirstOrDefault(x => x.Code == groupCode);

                if (group != null)
                {
                    asset.AssetGroupId = group.Id;
                }
                else
                {
                    // log error: AssetGroup for asset {name} not found by code={groupCode}
                }
            }
        }

        /// <summary>
        /// Deletes all lines before header
        /// </summary>
        /// <returns></returns>
        private IEnumerable<string> Clear(IEnumerable<string> lines)
        {
            var result = new List<string>();
            var headerFound = false;

            foreach (var line in lines)
            {
                if (line.Contains(LastLineIndicatorToTerminateCleaning))
                {
                    headerFound = true;
                }

                if (headerFound)
                {
                    result.Add(line);
                }
            }

            return result;
        }
    }
}
