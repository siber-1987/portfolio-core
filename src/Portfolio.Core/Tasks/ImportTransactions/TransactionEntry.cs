﻿using System;
using Portfolio.Core.Models;

namespace Portfolio.Core.Tasks.ImportTransactions
{
    public class TransactionEntry
    {
        public int Identifier { get; set; }

        public DateTime TransactionTime { get; set; }

        public string AssetName { get; set; }

        public TransactionType TransactionType { get; set; }

        public decimal PriceInCurrency { get; set; }

        public string Currency { get; set; }

        public decimal Commission { get; set; }

        public decimal Amount { get; set; }

        public int Volume { get; set; }
    }
}
