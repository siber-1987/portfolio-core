﻿using Portfolio.Core.Models;
using System;
using System.Globalization;

namespace Portfolio.Core.Tasks.ImportTransactions
{
    public interface ITransactionEntryParser
    {
        TransactionEntry Parse(string line);
    }

    public class TransactionEntryParser : ITransactionEntryParser
    {
        private const string DateTimeFormat = "dd.MM.yyyy HH:mm:ss";

        public TransactionEntry Parse(string line)
        {
            // TODO: Simplify parsing
            var items = line.Split(';');
            var time = DateTime.ParseExact(items[0], DateTimeFormat, CultureInfo.InvariantCulture);
            var volume = int.Parse(items[4].Replace(" ", ""));
            var priceStr = items[5].Replace(',', '.').Replace(" ", "");
            var price = decimal.Parse(priceStr, CultureInfo.InvariantCulture);
            var currency = items[6];
            var commissionStr = items[7].Replace(',', '.').Replace(" ", "");
            var commission = decimal.Parse(commissionStr, CultureInfo.InvariantCulture);
            var amountStr = items[9].Replace(',', '.').Replace(" ", "");
            var amount = decimal.Parse(amountStr, CultureInfo.InvariantCulture);

            return new TransactionEntry
            {
                TransactionTime = time,
                AssetName = items[1],
                TransactionType = GetTransactionType(items[3]),
                Volume = volume,
                PriceInCurrency = price,
                Currency = currency,
                Commission = commission,
                Amount = amount
            };
        }

        private TransactionType GetTransactionType(string transactionTypeStr)
        {
            return transactionTypeStr == "K" ? TransactionType.Buy : TransactionType.Sell;
        }
    }
}
