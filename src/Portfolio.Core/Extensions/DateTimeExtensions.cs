﻿using System;

namespace Portfolio.Core.Extensions
{
    public static class DateTimeExtensions
    {
        public static DateTime GetStartOfTheYear(this DateTime dateTime)
        {
            return new DateTime(dateTime.Year, 1, 1);
        }
    }
}
