﻿using System;

namespace Portfolio.Core.Models
{
    public class SplitMerge
    {
        public int Id { get; set; }

        public int AssetId { get; set; }

        public DateTime StateTime { get; set; }

        public DateTime ChangeTime { get; set; }

        public SplitMergeType SplitMergeType { get; set; }

        public int Before { get; set; }

        public int After { get; set; }
    }
}
