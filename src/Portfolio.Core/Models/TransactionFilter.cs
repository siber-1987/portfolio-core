﻿using System;

namespace Portfolio.Core.Models
{
    public class TransactionFilter
    {
        public int? AssetId { get; set; }
        public DateTime? DateFrom { get; set; }
        public DateTime? DateTo { get; set; }
    }
}
