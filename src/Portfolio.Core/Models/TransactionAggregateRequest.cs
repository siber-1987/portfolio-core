﻿namespace Portfolio.Core.Models
{
    // TODO: Add separate models with filter and with aggregation options
    public class TransactionAggregateRequest
    {
        public bool GroupByAsset { get; set; }

        public bool GroupByPeriod { get; set; }

        public PeriodType PeriodType { get; set; }

        public TransactionFilter TransactionFilter { get; set; }
    }
}
