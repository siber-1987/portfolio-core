﻿using System.Collections.Generic;

namespace Portfolio.Core.Models
{
    public class Asset
    {
        public int Id { get; set; }

        public int AssetGroupId { get; set; }

        public string Code { get; set; }

        public string Name { get; set; }

        public virtual ICollection<Transaction> Transactions { get; set; }
    }
}
