﻿using System;

namespace Portfolio.Core.Models
{
    /// <summary>
    /// Represents number of shares in portfolio at the certain date
    /// </summary>
    public class AssetHistoryEntry
    {
        public int AssetId { get; set; }

        public DateTime Date { get; set; }

        /// <summary>
        /// Number of shares
        /// </summary>
        public int Volume { get; set; }
    }
}
