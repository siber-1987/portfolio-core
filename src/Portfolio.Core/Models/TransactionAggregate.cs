﻿using System;

namespace Portfolio.Core.Models
{
    public class TransactionAggregate
    {
        public int? AssetId { get; set; }

        public string AssetName { get; set; }

        public DateTime? Period { get; set; }

        public decimal ExpensesSum { get; set; }

        public decimal IncomeSum { get; set; }

        public decimal Balance { get; set; }
    }
}
