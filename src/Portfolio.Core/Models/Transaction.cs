﻿using System;
using System.Collections.Generic;
using System.Linq;

namespace Portfolio.Core.Models
{
    public class Transaction
    {
        public int Id { get; set; }

        public int AssetId { get; set; }

        public string AssetName { get; set; }

        public DateTime TransactionTime { get; set; }

        public TransactionType TransactionType { get; set; }

        public int Volume { get; set; }

        public decimal PriceInCurrency { get; set; }

        public decimal Price
        {
            get
            {
                return Currency == "PLN" ? PriceInCurrency : Amount / Volume;
            }
        }

        public string Currency { get; set; }

        public decimal Commission { get; set; }

        public decimal Amount { get; set; }

        /// <summary>
        /// Buy and Sell operations' commisions sum
        /// </summary>
        public decimal CommissionsSum { get; set; }

        /// <summary>
        /// (Sell transactions amount) - (buy transactions amount) - commissions
        /// </summary>
        public decimal Balance { get; set; }

        public int SoldCounter { get; set; }

        public List<Transaction> BuyTransactions { get; set; }

        public string BuyTransactionIds
        {
            get
            {
                if (BuyTransactions == null || BuyTransactions.Count == 0)
                {
                    return string.Empty;
                }

                var ids = BuyTransactions
                    .Select(x => x.Id);

                return string.Join(",", ids);
            }
        }
    }
}
