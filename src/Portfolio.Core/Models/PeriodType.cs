﻿namespace Portfolio.Core.Models
{
    public enum PeriodType
    {
        Year = 0,
        Month = 1,
        Day = 2
    }
}
