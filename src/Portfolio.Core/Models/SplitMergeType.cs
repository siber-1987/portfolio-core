﻿namespace Portfolio.Core.Models
{
    public enum SplitMergeType
    {
        Split = 0,
        Merge = 1
    }
}
