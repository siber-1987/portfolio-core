﻿namespace Portfolio.Core.Models
{
    public enum TransactionType
    {
        Buy = 0,
        Sell = 1
    }
}
