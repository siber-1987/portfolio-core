﻿using System.Collections.Generic;
using System.IO;

namespace Portfolio.Core.Utils
{
    public interface IFileReader
    {
        IEnumerable<string> ReadLines(string fileName);
    }

    public class FileReader : IFileReader
    {
        public IEnumerable<string> ReadLines(string fileName)
        {
            var lines = new List<string>();
            using (var sr = new StreamReader(fileName))
            {
                var line = "";
                while ((line = sr.ReadLine()) != null)
                {
                    lines.Add(line);
                }
            }

            return lines;
        }
    }
}
