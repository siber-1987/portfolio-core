﻿using System.Text.RegularExpressions;

namespace Portfolio.Core.Utils
{
    public interface IAssetGroupCreator
    {
        string GetGroupName(string assetName);
        string GetGroupCode(string assetName);
    }

    public class AssetGroupCreator : IAssetGroupCreator
    {
        private const string CertificateLabel = "Certificate";
        private static readonly Regex CertificateRegex = new Regex("^(INT)([LS])([A-Z0-9]{3})([0-9]{5})$");

        public string GetGroupName(string assetName)
        {
            if (CertificateRegex.IsMatch(assetName))
            {
                return $"{CertificateLabel} {GetGroupCode(assetName)}";
            }
            else
            {
                return assetName;
            }
        }

        public string GetGroupCode(string assetName)
        {
            if (CertificateRegex.IsMatch(assetName))
            {
                var match = CertificateRegex.Match(assetName);
                if (match.Groups.Count == 5)
                {
                    return match.Groups[3].Value;
                }
            }

            return assetName;
        }
    }
}
