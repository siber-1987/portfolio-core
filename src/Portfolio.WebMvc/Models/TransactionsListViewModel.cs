﻿using Portfolio.Core.Models;

namespace Portfolio.WebMvc.Models
{
    public class TransactionsListViewModel
    {
        public List<Transaction>? Transactions { get; set; }
        public List<Asset>? Assets { get; set; }
        public TransactionFilter? Filter { get; set; }
    }
}
