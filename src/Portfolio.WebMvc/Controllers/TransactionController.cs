﻿using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.Rendering;
using Portfolio.Core.Services;
using Portfolio.Core.Models;
using Portfolio.WebMvc.Models;
using Portfolio.Core.Extensions;

namespace Portfolio.WebMvc.Controllers
{
    public class TransactionController : Controller
    {
        private readonly ITransactionsService _transactionsService;
        private readonly IAssetsService _assetsService;

        public TransactionController(
            ITransactionsService transactionsService,
            IAssetsService assetsService)
        {
            _transactionsService = transactionsService;
            _assetsService = assetsService;
        }

        public IActionResult Index()
        {
            return View();
        }

        public ViewResult List(TransactionsListViewModel inputModel)
        {
            if (inputModel.Filter == null)
            {
                inputModel.Filter = new TransactionFilter
                {
                    DateFrom = DateTime.Today.GetStartOfTheYear()
                };
            }

            var transactions = _transactionsService.GetTransactions(inputModel.Filter);
            var assets = _assetsService.GetAssets();

            ViewBag.Assets = new SelectList(assets);

            var model = new TransactionsListViewModel
            {
                Transactions = transactions.ToList(),  
                Assets = assets.ToList(),
                Filter = inputModel.Filter
            };

            return View(model);
        }

        public ViewResult GetAssetsSummary()
        {
            var filter = new TransactionAggregateRequest
            {
                GroupByAsset = true
            };

            var data = _transactionsService.GetTransactionAggregate(filter);

            return View("AssetsSummary", data);
        }

        /// <summary>
        /// Sets some default values for filter in case it selects too much data.
        /// </summary>
        /// <param name="filter"></param>
        private void InitFilter(TransactionFilter filter)
        {
            if (filter != null)
            {
                return;
            }

            filter = new TransactionFilter
            {
                DateFrom = DateTime.Today.GetStartOfTheYear()
            };
        }
    }
}
