﻿using Microsoft.AspNetCore.Mvc;
using Portfolio.Core.Services;

namespace Portfolio.WebMvc.Controllers
{
    public class AssetController : Controller
    {
        private readonly IAssetsService _assetsService;

        public AssetController(IAssetsService assetsService)
        {
            _assetsService = assetsService;
        }

        public IActionResult Index()
        {
            return View();
        }

        public ViewResult List()
        {
            var assets = _assetsService.GetAssets();

            //var model = assets.Select(x => new Asset
            //{
            //    Id = x.Id,
            //    AssetGroupId = x.AssetGroupId,
            //    Code = x.Code,
            //    Name = x.Name
            //}).ToList();

            return View(assets);
        }
    }
}
