﻿using Portfolio.Data.Models;
using System.Linq;

namespace Portfolio.Data.Repositories
{
    public interface ISplitMergeRepository
    {
        void Add(SplitMerge[] items);
        IQueryable<SplitMerge> GetAll();
    }
}
