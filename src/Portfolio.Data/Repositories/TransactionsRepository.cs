﻿using System.Linq;
using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Internal;
using Portfolio.Data.Models;

namespace Portfolio.Data.Repositories
{
    public class TransactionsRepository : ITransactionsRepository
    {
        private readonly PortfolioContext _portfolioContext;

        public TransactionsRepository(PortfolioContext portfolioContext)
        {
            _portfolioContext = portfolioContext;
        }

        public void AddTransactions(Transaction[] transactions)
        {
            _portfolioContext.Transactions.AddRange(transactions);
            _portfolioContext.SaveChanges();
        }

        public IQueryable<Transaction> GetTransactions()
        {
            return _portfolioContext
                .Transactions
                .Include(x => x.Asset)
                .AsQueryable();
        }

        public Asset AddAsset(Asset asset)
        {
            _portfolioContext.Assets.Add(asset);
            _portfolioContext.SaveChanges();

            return asset;
        }

        public void AddAssets(Asset[] assets)
        {
            _portfolioContext.Assets.AddRange(assets);
            _portfolioContext.SaveChanges();
        }

        public IQueryable<Asset> GetAssets()
        {
            return _portfolioContext.Assets.AsQueryable();
        }

        public AssetGroup AddAssetGroup(AssetGroup assetGroup)
        {
            _portfolioContext.AssetGroups.Add(assetGroup);
            _portfolioContext.SaveChanges();

            return assetGroup;
        }

        public void AddAssetGroups(AssetGroup[] assetGroups)
        {
            _portfolioContext.AssetGroups.AddRange(assetGroups);
            _portfolioContext.SaveChanges();
        }

        public IQueryable<AssetGroup> GetAssetGroups()
        {
            return _portfolioContext.AssetGroups.AsQueryable();
        }
    }
}
