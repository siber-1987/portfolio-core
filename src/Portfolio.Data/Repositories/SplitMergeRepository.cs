﻿using Portfolio.Data.Models;
using System.Linq;

namespace Portfolio.Data.Repositories
{
    public class SplitMergeRepository : ISplitMergeRepository
    {
        private readonly PortfolioContext _portfolioContext;

        public SplitMergeRepository(PortfolioContext portfolioContext)
        {
            _portfolioContext = portfolioContext;
        }

        public void Add(SplitMerge[] items)
        {
            _portfolioContext.SplitMerges.AddRange(items);
            _portfolioContext.SaveChanges();
        }

        public IQueryable<SplitMerge> GetAll()
        {
            return _portfolioContext.SplitMerges.AsQueryable();
        }

    }
}
