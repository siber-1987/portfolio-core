﻿using System.Linq;
using Portfolio.Data.Models;

namespace Portfolio.Data.Repositories
{
    public interface ITransactionsRepository
    {
        void AddTransactions(Transaction[] transactions);
        IQueryable<Transaction> GetTransactions();

        Asset AddAsset(Asset asset);
        void AddAssets(Asset[] assets);
        IQueryable<Asset> GetAssets();

        AssetGroup AddAssetGroup(AssetGroup assetGroup);
        void AddAssetGroups(AssetGroup[] assetGroup);
        IQueryable<AssetGroup> GetAssetGroups();
    }
}
