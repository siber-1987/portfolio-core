﻿using System;
using System.ComponentModel.DataAnnotations.Schema;

namespace Portfolio.Data.Models
{
    public class Transaction
    {
        public int Id { get; set; }

        public int Identifier { get; set; }

        public int AssetId { get; set; }

        public DateTime TransactionTime { get; set; }

        public int TransactionType { get; set; }

        public int Volume { get; set; }

        [Column(TypeName = "decimal")]
        public decimal PriceInCurrency { get; set; }

        public string Currency { get; set; }

        [Column(TypeName = "decimal")]
        public decimal Commission { get; set; }

        [Column(TypeName = "decimal")]
        public decimal Amount { get; set; }

        public virtual Asset Asset { get; set; }
    }
}
