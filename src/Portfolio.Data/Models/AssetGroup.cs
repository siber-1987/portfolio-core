﻿using System.Collections.Generic;

namespace Portfolio.Data.Models
{
    public class AssetGroup
    {
        public int Id { get; set; }
        public string Code { get; set; }
        public string Name { get; set; }
        public virtual ICollection<Asset> Assets { get; set; }
    }
}
