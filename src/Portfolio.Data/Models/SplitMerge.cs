﻿using System;

namespace Portfolio.Data.Models
{
    public class SplitMerge
    {
        public int Id { get; set; }

        public int Identifier { get; set; }

        public int AssetId { get; set; }

        public DateTime StateTime { get; set; }

        public DateTime ChangeTime { get; set; }

        public int SplitMergeType { get; set; }

        public int Before { get; set; }

        public int After { get; set; }
    }
}
