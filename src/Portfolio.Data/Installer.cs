﻿using System;
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;
using Portfolio.Data.Repositories;

namespace Portfolio.Data
{
    public static class Installer
    {
        public static void AddPortfolioDataServices(this IServiceCollection services, IConfiguration configuration)
        {
            services.AddTransient<ITransactionsRepository, TransactionsRepository>();
            services.AddTransient<ISplitMergeRepository, SplitMergeRepository>();

            services.AddDbContext<PortfolioContext>(options =>
            {
                options.UseSqlServer(
                    configuration["ConnectionString"],
                    sqlServerOptionsAction: sqlOptions =>
                    {
                        sqlOptions.EnableRetryOnFailure(maxRetryCount: 10, maxRetryDelay: TimeSpan.FromSeconds(30), errorNumbersToAdd: null);
                    });
            });
        }
    }
}
