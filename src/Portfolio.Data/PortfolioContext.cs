﻿using Microsoft.EntityFrameworkCore;
using Portfolio.Data.Models;

namespace Portfolio.Data
{
    public class PortfolioContext : DbContext
    {
        public PortfolioContext()
        {
        }

        public PortfolioContext(DbContextOptions<PortfolioContext> options)
            : base(options)
        {
        }

        public DbSet<AssetGroup> AssetGroups { get; set; }
        public DbSet<Asset> Assets { get; set; }
        public DbSet<Transaction> Transactions { get; set; }
        public DbSet<SplitMerge> SplitMerges { get; set; }
    }
}
