CREATE TABLE [dbo].[AssetGroups](
	[Id] int NOT NULL IDENTITY(1,1),
	[Code] [nvarchar](30) NOT NULL,
	[Name] [nvarchar](50) NOT NULL,
	CONSTRAINT [PK_AssetGroups] PRIMARY KEY CLUSTERED 
	(
		[Id] ASC
	) WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY],
) ON [PRIMARY]
GO

CREATE TABLE [dbo].[Assets](
	[Id] int NOT NULL IDENTITY(1,1),
	[AssetGroupId] int NULL,
	[Code] [nvarchar](30) NOT NULL,
	[Name] [nvarchar](50) NOT NULL,
	CONSTRAINT [PK_Assets] PRIMARY KEY CLUSTERED 
	(
		[Id] ASC
	) WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY],
) ON [PRIMARY]
GO

ALTER TABLE [Assets]
ADD CONSTRAINT [FK_Assets_AssetGroups]
FOREIGN KEY ([AssetGroupId]) REFERENCES [AssetGroups]([Id]);

CREATE TABLE [dbo].[Transactions](
	[Id] int NOT NULL IDENTITY(1,1),
	[Identifier] int NOT NULL,
	[AssetId] int NOT NULL,
	[TransactionTime] [datetime2](7) NOT NULL,
	[TransactionType] [int] NOT NULL,
	[Volume] [int] NOT NULL,
	[PriceInCurrency] [decimal](19,8) NOT NULL,
	[Currency] [nchar](3) NOT NULL,
	[Commission] [decimal](19,8) NOT NULL,
	[Amount] [decimal](19,8) NOT NULL,
	CONSTRAINT [PK_Transactions] PRIMARY KEY CLUSTERED
	(
		[Id] ASC
	) WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY],
	CONSTRAINT [U_Transactions] UNIQUE (Identifier)
) ON [PRIMARY]
GO

ALTER TABLE [Transactions]
ADD CONSTRAINT [FK_Transactions_Assets]
FOREIGN KEY ([AssetId]) REFERENCES [Assets]([Id]);

CREATE NONCLUSTERED INDEX [IX_Tramsactions_TransactionTime] ON [Transactions]
(
	[TransactionTime] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, SORT_IN_TEMPDB = OFF, DROP_EXISTING = OFF, ONLINE = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON)
GO

CREATE NONCLUSTERED INDEX [IX_Transactions_AssetId_TransactionTime] ON [dbo].[Transactions]
(
	[AssetId] ASC,
	[TransactionTime] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, SORT_IN_TEMPDB = OFF, DROP_EXISTING = OFF, ONLINE = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON)
GO

CREATE TABLE [dbo].[SplitMerges](
	[Id] int NOT NULL IDENTITY(1,1),
	[Identifier] int NOT NULL,
	[AssetId] int NOT NULL,
	[StateTime] [datetime2](7) NOT NULL,
	[ChangeTime] [datetime2](7) NOT NULL,
	[SplitMergeType] [int] NOT NULL,
	[Before] [int] NOT NULL,
	[After] [int] NOT NULL,
	CONSTRAINT [PK_SplitMerges] PRIMARY KEY CLUSTERED
	(
		[Id] ASC
	) WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY],
	CONSTRAINT [U_SplitMerges] UNIQUE (Identifier)
) ON [PRIMARY]
GO

ALTER TABLE [SplitMerges]
ADD CONSTRAINT [FK_SplitMerges_Assets]
FOREIGN KEY ([AssetId]) REFERENCES [Assets]([Id]);