# portfolio-core

## About Solution
Repository contains the following components:
- Portfolio Agent
- Portfolio API

## Preparing environment

1. Install chocolatey from `https://docs.chocolatey.org/en-us/choco/setup`
2. Install `psake:
```
choco install psake
```
3. Install Docker Desktop
4. Install powershell modules
```
psake install-modules
```

## Setup database
1. Start `sql` container with SqlServer:
```
psake start-sqlserver
```
It will start SqlServer 2022 container:
- Server name: `localhost,1433`
- Admin user: `sa`
- Admin password: `p-aSSw0rd`
2. Create schema:
```
psake build-db
```

## Starting
To publish solution, build docker images and run solution in containers:
```
psake init
```

## Building solution
To build with psake:
```
psake build
```

## Building docker images
To build images from cmd:
```
docker build -t portfolio-api ./src/Portfolio.Api
docker build -t portfolio-agent ./src/Portfolio.Agent
```

## Running solution
To run Api from cmd:
```
cd src\Portfolio.Api
dotnet .\Portfolio.Api.dll
```